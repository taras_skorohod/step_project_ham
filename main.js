

let menuList = document.querySelector('.ourservices-tabs');
let textList = document.querySelectorAll('.tabs-content>div');
let titleTabs = document.querySelectorAll('.ourservices-tab-header');

menuList.addEventListener('click', (event) => {
	titleTabs.forEach((elem) => {
		elem.classList.remove('tab-active');
	});

	let target = event.target;

	if (target.className === 'ourservices-tab-header') {
		target.classList.add('tab-active');

		textList.forEach((e) => {
			if (e.id.split('-').join('') === target.innerHTML.split(' ').join('').toLowerCase()) {
				e.style.display = 'flex';
			} else {
				e.style.display = 'none';
			}
		});
	}
});


let lastDataFilter = 'all';
const pageSize = 12;
let pageOffset = 0;
const POSTS = [
	[ 'graphic-design', 'Graphic Design', 1 ],
	[ 'web-design', 'Web Design', 1 ],
	[ 'landing-pages', 'Landing Pages', 1 ],
	[ 'wordpress', 'Wordpress', 1 ],
	[ 'landing-pages', 'Landing Pages', 2 ],
	[ 'wordpress', 'Wordpress', 2 ],
	[ 'graphic-design', 'Graphic Design', 2 ],
	[ 'web-design', 'Web Design', 2 ],
	[ 'landing-pages', 'Landing Pages', 3 ],
	[ 'wordpress', 'Wordpress', 3 ],
	[ 'graphic-design', 'Graphic Design', 3 ],
	[ 'web-design', 'Web Design', 3 ],
	[ 'graphic-design', 'Graphic Design', 4 ],
	[ 'web-design', 'Web Design', 4 ],
	[ 'landing-pages', 'Landing Pages', 4 ],
	[ 'wordpress', 'Wordpress', 4 ],
	[ 'landing-pages', 'Landing Pages', 5 ],
	[ 'wordpress', 'Wordpress', 5 ],
	[ 'graphic-design', 'Graphic Design', 5 ],
	[ 'web-design', 'Web Design', 5 ],
	[ 'landing-pages', 'Landing Pages', 6 ],
	[ 'wordpress', 'Wordpress', 6 ],
	[ 'graphic-design', 'Graphic Design', 6 ],
	[ 'web-design', 'Web Design', 6 ],
	[ 'graphic-design', 'Graphic Design', 7 ],
	[ 'web-design', 'Web Design', 7 ],
	[ 'landing-pages', 'Landing Pages', 7 ],
	[ 'wordpress', 'Wordpress', 7 ],
	[ 'landing-pages', 'Landing Pages', 8 ],
	[ 'wordpress', 'Wordpress', 8 ],
	[ 'graphic-design', 'Graphic Design', 8 ],
	[ 'web-design', 'Web Design', 8 ],
	[ 'landing-pages', 'Landing Pages', 9 ],
	[ 'wordpress', 'Wordpress', 9 ],
	[ 'graphic-design', 'Graphic Design', 9 ],
	[ 'web-design', 'Web Design', 9 ],
];

const injectPost = (post) => {
	const postContainer = document.querySelector('.ourwork-gallery-block');
	postContainer.appendChild(post);
	console.log(post);
};

const createPost = (className, value, tabName) => {
	const galleryPost = createElement('div', `ourwork-gallery-block-item ${className}`);
	const image = createElement('img', 'work-photo');
	image.src = `./imgs/ourwork/${className}/${className}${value}.jpg`;
	galleryPost.appendChild(image);
	galleryPost.appendChild(createHover(tabName));

	return galleryPost;
};

const showPosts = () => {
	POSTS.slice(pageOffset, pageSize + pageOffset).forEach((post) => {
		const { 0: className, 1: tabName, 2: value } = post;
		injectPost(createPost(className, value, tabName));
	});
	console.log(POSTS.slice(pageOffset, pageSize));
	pageOffset += pageSize;
	if (pageOffset >= POSTS.length) {
		document.querySelector('#loadmore').style.display = 'none';
	}
	filterData();
};

const createHover = (tabName) => {
	const creativeDesignContainer = createElement('div', 'creaive_design-container');
	const creativeDesignSvg = createElement('div', 'creative_design-svgs');
	creativeDesignContainer.appendChild(creativeDesignSvg);
	creativeDesignSvg.appendChild(createCircleWrapper('link-icon'));
	creativeDesignSvg.appendChild(createCircleWrapper('square-icon'));

	const pContainer = createElement('div');
	const p1 = createElement('p', 'creaive_design-text1', 'creative design');
	const p2 = createElement('p', 'creaive_design-text2', `${tabName}`);
	pContainer.appendChild(p1);
	pContainer.appendChild(p2);
	creativeDesignContainer.appendChild(pContainer);

	return creativeDesignContainer;
};

const createCircleWrapper = (className) => {
	const aHref = createElement('a', 'circle circle__wrapper');
	aHref.href = '#void';
	const icon = createElement('i', `${className}`);
	aHref.appendChild(icon);

	return aHref;
};

function createElement(element, className = '', text = '') {
	const el = document.createElement(element);
	el.className = className;
	el.innerText = text;

	return el;
}

showPosts();

const onClickHandler = () => {
	const loadMore = document.querySelector('#loadmore');
	loadMore.addEventListener('click', () => {
		console.log('w');
		showPosts();
	});
};
onClickHandler();

function filterData(filterValue = lastDataFilter) {
	const galleryItems = document.querySelectorAll('.ourwork-gallery-block-item');
	galleryItems.forEach((item) => {
		if (item.classList.contains(filterValue) || filterValue === 'all') {
			item.classList.remove('hide');
			item.classList.add('show');
		} else {
			item.classList.remove('show');
			item.classList.add('hide');
		}
	});
	lastDataFilter = filterValue;
}

const filterContainer = document.querySelector('.ourwork-selector');
filterContainer.addEventListener('click', (event) => {
	if (event.target.classList.contains('ourwork-type')) {

		filterContainer.querySelector('.active').classList.remove('active');

		event.target.classList.add('active');
		const filterValue = event.target.getAttribute('data-filter');
		filterData(filterValue);
		console.log(filterValue);
	}
});


let mySwiper = new Swiper('.swiper-container', {
	direction: 'horizontal',

	pagination: {
		el: '.swiper-pagination',
		clickable: true,
		renderBullet: function(index, className) {
			let src = this.slides[index].querySelector('.about-ham-img').getAttribute('src');
			return `<img class="${className}" src="${src}" alt="Photo">`;
		},
	},

	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
});



